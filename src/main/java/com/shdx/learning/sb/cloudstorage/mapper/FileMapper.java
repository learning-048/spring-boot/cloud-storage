package com.shdx.learning.sb.cloudstorage.mapper;

import com.shdx.learning.sb.cloudstorage.model.File;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface FileMapper {

    @Insert("INSERT INTO FILES (filename, contenttype, filesize, userid, filedata) VALUES(#{filename}, #{contentType}, #{fileSize}, #{userId}, #{fileData})")
    @Options(useGeneratedKeys = true, keyProperty = "fileId")
    int insertFile(File file);

    @Select("SELECT * FROM FILES WHERE filename = #{filename} AND userid = #{userId}")
    File getFile(String filename, int userId);

    @Select("SELECT filename FROM FILES WHERE userid = #{userId}")
    String[] getFileNames(int userId);

    @Delete("DELETE FROM FILES WHERE filename = #{filename} AND userid = #{userId}")
    void deleteFile(String filename, int userId);

}