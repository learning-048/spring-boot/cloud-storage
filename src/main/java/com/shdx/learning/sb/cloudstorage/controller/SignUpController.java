package com.shdx.learning.sb.cloudstorage.controller;

import com.shdx.learning.sb.cloudstorage.model.User;
import com.shdx.learning.sb.cloudstorage.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signup")
public class SignUpController {

    private final UserService userService;

    public SignUpController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getSignUpView() {
        return "signup";
    }

    @PostMapping
    public String signUpUser(@ModelAttribute User user, Model model) {
        String signUpError = null;

        if (!userService.isUsernameAvailable(user.getUsername())) {
            signUpError = "The username already exists.";
        }

        if (signUpError == null) {
            int rowsAdded = userService.createUser(user);

            if (rowsAdded < 0) {
                signUpError = "There was an error signing you up. Please try again.";
            }
        }

        if (signUpError == null) {
            model.addAttribute("signupSuccess", true);
        } else {
            model.addAttribute("signUpError", signUpError);
        }

        return "signup";
    }

}
