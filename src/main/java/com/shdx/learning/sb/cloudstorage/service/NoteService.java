package com.shdx.learning.sb.cloudstorage.service;

import com.shdx.learning.sb.cloudstorage.mapper.NoteMapper;
import com.shdx.learning.sb.cloudstorage.model.Note;
import org.springframework.stereotype.Service;

@Service
public class NoteService {

    private final NoteMapper noteMapper;

    public NoteService(NoteMapper noteMapper) {
        this.noteMapper = noteMapper;
    }

    public void saveNote(Note note) {
        noteMapper.insertNote(note);
    }

    public void editNote(Note note) {
        noteMapper.updateNote(note);
    }

    public Note[] getNotes(int userId) {
        return noteMapper.getNotes(userId);
    }

    public void deleteNote(int noteId) {
        noteMapper.deleteNote(noteId);
    }

}
