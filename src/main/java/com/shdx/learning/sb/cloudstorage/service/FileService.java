package com.shdx.learning.sb.cloudstorage.service;

import com.shdx.learning.sb.cloudstorage.mapper.FileMapper;
import com.shdx.learning.sb.cloudstorage.model.File;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;

@Service
public class FileService {

    private final FileMapper fileMapper;

    public FileService(FileMapper fileMapper) {
        this.fileMapper = fileMapper;
    }

    public void addFile(MultipartFile multipartFile, int userId) {
        try {
            fileMapper.insertFile(new File(null, multipartFile.getOriginalFilename(), multipartFile.getContentType(),
                    String.valueOf(multipartFile.getSize()), userId, multipartFile.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[] getFileNames(int userId) {
        return fileMapper.getFileNames(userId);
    }

    public byte[] getFileData(String filename, int userId) {
        return fileMapper.getFile(filename, userId).getFileData();
    }

    public void deleteFile(String filename, int userId) {
        fileMapper.deleteFile(filename, userId);
    }

    public boolean fileExists(String filename, int userId) {
        return Arrays.asList(fileMapper.getFileNames(userId)).contains(filename);
    }

}
