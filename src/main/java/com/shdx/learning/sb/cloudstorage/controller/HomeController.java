package com.shdx.learning.sb.cloudstorage.controller;

import com.shdx.learning.sb.cloudstorage.model.Credential;
import com.shdx.learning.sb.cloudstorage.model.Note;
import com.shdx.learning.sb.cloudstorage.service.CredentialService;
import com.shdx.learning.sb.cloudstorage.service.FileService;
import com.shdx.learning.sb.cloudstorage.service.NoteService;
import com.shdx.learning.sb.cloudstorage.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@ControllerAdvice
public class HomeController {

    private static final String HOME_PATH = "home";
    private static final String REDIRECT_HOME_PATH = "redirect:/home";
    private static final String ACTIVE_TAB_ATTRIBUTE = "activeTab";
    private final UserService userService;
    private final FileService fileService;
    private final NoteService noteService;
    private final CredentialService credentialService;
    private int userId;

    public HomeController(UserService userService, FileService fileService, NoteService noteService, CredentialService credentialService) {
        this.userService = userService;
        this.fileService = fileService;
        this.noteService = noteService;
        this.credentialService = credentialService;
    }

    @GetMapping("/home")
    public String getHomeView(Authentication auth, Note note, Credential credential, @ModelAttribute(ACTIVE_TAB_ATTRIBUTE) String activeTab, Model model) {
        userId = userService.getUser(auth.getName()).getUserId();
        model.addAttribute("fileNames", fileService.getFileNames(userId));
        model.addAttribute("notes", noteService.getNotes(userId));
        model.addAttribute("credentials", credentialService.getCredentials(userId));
        model.addAttribute("credentialService", credentialService);

        if (activeTab.isEmpty()) {
            model.addAttribute(ACTIVE_TAB_ATTRIBUTE, "files");
        }

        return HOME_PATH;
    }

    @GetMapping("/file-view")
    public ResponseEntity<byte[]> handleFileView(@RequestParam String fileName) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(fileService.getFileData(fileName, userId));
    }

    @GetMapping("/file-delete")
    public String handleFileDelete(@RequestParam String fileName, RedirectAttributes redirectAttributes) {
        fileService.deleteFile(fileName, userId);
        redirectAttributes.addFlashAttribute("fileDeleted", true);
        
        return REDIRECT_HOME_PATH;
    }

    @PostMapping("/file-upload")
    public String handleFileUpload(@RequestParam("fileUpload") MultipartFile multipartFile, RedirectAttributes redirectAttributes) {
        boolean fileExists = fileService.fileExists(multipartFile.getOriginalFilename(), userId);

        if (fileExists) {
            redirectAttributes.addFlashAttribute("fileExists", true);
        } else if (!multipartFile.isEmpty()) {
            fileService.addFile(multipartFile, userId);
            redirectAttributes.addFlashAttribute("fileUploaded", true);
        }

        return REDIRECT_HOME_PATH;
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleFileUploadError(RedirectAttributes redirectAttributes){
        redirectAttributes.addFlashAttribute("fileSizeExceedsLimit", true);

        return REDIRECT_HOME_PATH;
    }

    @PostMapping("/note-save")
    public String handleNoteSave(Note note, RedirectAttributes redirectAttributes) {
        note.setUserId(userId);

        if (note.getNoteId() == null) {
            noteService.saveNote(note);
        } else {
            noteService.editNote(note);
        }

        redirectAttributes.addFlashAttribute("noteSaved", true);
        redirectAttributes.addFlashAttribute(ACTIVE_TAB_ATTRIBUTE, "notes");

        return REDIRECT_HOME_PATH;
    }

    @GetMapping("/note-delete")
    public String handleNoteDelete(@RequestParam int noteId, RedirectAttributes redirectAttributes) {
        noteService.deleteNote(noteId);

        redirectAttributes.addFlashAttribute("noteDeleted", true);
        redirectAttributes.addFlashAttribute(ACTIVE_TAB_ATTRIBUTE, "notes");

        return REDIRECT_HOME_PATH;
    }

    @PostMapping("/credential-save")
    public String handleCredentialSave(Credential credential, RedirectAttributes redirectAttributes) {
        credential.setUserId(userId);

        if (credential.getCredentialId() == null) {
            credentialService.saveCredential(credential);
        } else {
            credentialService.editCredential(credential);
        }

        redirectAttributes.addFlashAttribute("credentialSaved", true);
        redirectAttributes.addFlashAttribute(ACTIVE_TAB_ATTRIBUTE, "credentials");

        return REDIRECT_HOME_PATH;
    }

    @GetMapping("/credential-delete")
    public String handleCredentialDelete(@RequestParam int credentialId, RedirectAttributes redirectAttributes) {
        credentialService.deleteCredential(credentialId);

        redirectAttributes.addFlashAttribute("credentialDeleted", true);
        redirectAttributes.addFlashAttribute(ACTIVE_TAB_ATTRIBUTE, "credentials");

        return REDIRECT_HOME_PATH;
    }

}
