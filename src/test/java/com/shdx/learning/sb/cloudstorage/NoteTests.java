package com.shdx.learning.sb.cloudstorage;

import com.shdx.learning.sb.cloudstorage.page.HomePage;
import com.shdx.learning.sb.cloudstorage.page.LoginPage;
import com.shdx.learning.sb.cloudstorage.page.SignUpPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class NoteTests {

    private static final String BASE_URL = "http://localhost";
    private static final String LOGIN_PATH = "/login";
    private static final String SIGNUP_PATH = "/signup";
    private static final String NEW_NOTE_TITLE = "New Note";
    private static final String NEW_NOTE_DESCRIPTION = "A new note";
    private static final String EDITED_NOTE_TITLE = "Edited Note";
    private static final String EDITED_NOTE_DESCRIPTION = "An edited note";
    private static WebDriver driver;
    private static HomePage homePage;

    @LocalServerPort
    private int port;

    @BeforeAll
    public void beforeAll() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        homePage = new HomePage(driver);

        // Sign up and log in
        SignUpPage signUpPage = new SignUpPage(driver);
        LoginPage loginPage = new LoginPage(driver);

        driver.get(BASE_URL + ":" + port + SIGNUP_PATH);
        signUpPage.signUp("Note", "Doe", "note.doe", "123");

        driver.get(BASE_URL + ":" + port + LOGIN_PATH);
        loginPage.login("note.doe", "123");
    }

    @AfterAll
    public void afterAll() {
        driver.quit();
    }

    @Test
    void testAddNote() {
        // Add note
        homePage.addNote(NEW_NOTE_TITLE, NEW_NOTE_DESCRIPTION);
        WebElement noteTable = driver.findElement(By.id("noteTable"));

        // Assertions
        Assertions.assertTrue(noteTable.findElement(By.tagName("tbody")).getText().contains(NEW_NOTE_TITLE));
        Assertions.assertTrue(noteTable.findElement(By.tagName("tbody")).getText().contains(NEW_NOTE_DESCRIPTION));

        // Clean up
        homePage.deleteNote();
    }

    @Test
    void testEditNote() {
        // Add note
        homePage.addNote(NEW_NOTE_TITLE, NEW_NOTE_DESCRIPTION);

        // Edit note
        homePage.editNote(EDITED_NOTE_TITLE, EDITED_NOTE_DESCRIPTION);
        WebElement noteTable = driver.findElement(By.id("noteTable"));

        // Assertions
        Assertions.assertTrue(noteTable.findElement(By.tagName("tbody")).getText().contains(EDITED_NOTE_TITLE));
        Assertions.assertTrue(noteTable.findElement(By.tagName("tbody")).getText().contains(EDITED_NOTE_DESCRIPTION));

        // Clean up
        homePage.deleteNote();
    }

    @Test
    void testDeleteNote() {
        // Add note
        homePage.addNote(NEW_NOTE_TITLE, NEW_NOTE_DESCRIPTION);
        WebElement noteTable = driver.findElement(By.id("noteTable"));

        // Delete note
        homePage.deleteNote();

        // Assertion
        Assertions.assertThrows(StaleElementReferenceException.class, () -> noteTable.findElement(By.tagName("tbody")));
    }

}
