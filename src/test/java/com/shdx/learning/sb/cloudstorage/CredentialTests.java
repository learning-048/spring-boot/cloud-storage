package com.shdx.learning.sb.cloudstorage;

import com.shdx.learning.sb.cloudstorage.page.HomePage;
import com.shdx.learning.sb.cloudstorage.page.LoginPage;
import com.shdx.learning.sb.cloudstorage.page.SignUpPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CredentialTests {

    private static final String BASE_URL = "http://localhost";
    private static final String LOGIN_PATH = "/login";
    private static final String SIGNUP_PATH = "/signup";
    private static final String NEW_CRED_URL = "www.new-cred-url.com";
    private static final String NEW_USERNAME = "A new username";
    private static final String NEW_PASSWORD = "A new password";
    private static final String EDITED_URL = "www.edited-url.com";
    private static final String EDITED_USERNAME = "An edited username";
    private static final String EDITED_PASSWORD = "An edited password";
    private static WebDriver driver;
    private static HomePage homePage;

    @LocalServerPort
    private int port;

    @BeforeAll
    public void beforeAll() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        homePage = new HomePage(driver);

        // Sign up and log in
        SignUpPage signUpPage = new SignUpPage(driver);
        LoginPage loginPage = new LoginPage(driver);

        driver.get(BASE_URL + ":" + port + SIGNUP_PATH);
        signUpPage.signUp("Credential", "Doe", "cred.doe", "123");

        driver.get(BASE_URL + ":" + port + LOGIN_PATH);
        loginPage.login("cred.doe", "123");
    }

    @AfterAll
    public void afterAll() {
        driver.quit();
    }

    @Test
    void testAddCredential() {
        // Add credential
        homePage.addCredential(NEW_CRED_URL, NEW_USERNAME, NEW_PASSWORD);
        WebElement credentialTable = driver.findElement(By.id("credentialTable"));

        // Assertions
        Assertions.assertTrue(credentialTable.findElement(By.tagName("tbody")).getText().contains(NEW_CRED_URL));
        Assertions.assertTrue(credentialTable.findElement(By.tagName("tbody")).getText().contains(NEW_USERNAME));
        Assertions.assertFalse(credentialTable.findElement(By.tagName("tbody")).getText().contains(NEW_PASSWORD));

        // Clean up
        homePage.deleteCredential();
    }

    @Test
    void testEditCredential() throws InterruptedException {
        // Add credential
        homePage.addCredential(NEW_CRED_URL, NEW_USERNAME, NEW_PASSWORD);

        // Edit credential
        homePage.editCredential(EDITED_URL, EDITED_USERNAME, EDITED_PASSWORD);
        WebElement credentialTable = driver.findElement(By.id("credentialTable"));

        // Assertions
        Assertions.assertTrue(credentialTable.findElement(By.tagName("tbody")).getText().contains(EDITED_URL));
        Assertions.assertTrue(credentialTable.findElement(By.tagName("tbody")).getText().contains(EDITED_USERNAME));
        Assertions.assertFalse(credentialTable.findElement(By.tagName("tbody")).getText().contains(EDITED_PASSWORD));

        homePage.openCredentialModal();
        Assertions.assertEquals(EDITED_PASSWORD, driver.findElement(By.id("credential-password")).getAttribute("value"));

        // Clean up
        homePage.closeCredentialModal();
        homePage.deleteCredential();
    }

    @Test
    void testDeleteCredential() {
        // Add credential
        homePage.addCredential(NEW_CRED_URL, NEW_USERNAME, NEW_PASSWORD);
        WebElement credentialTable = driver.findElement(By.id("credentialTable"));

        // Clean up
        homePage.deleteCredential();

        // Assertion
        Assertions.assertThrows(StaleElementReferenceException.class, () -> credentialTable.findElement(By.tagName("tbody")));
    }

}
