package com.shdx.learning.sb.cloudstorage.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {

    @FindBy(id = "inputFirstName")
    private WebElement inputFirstName;

    @FindBy(id = "inputLastName")
    private WebElement inputLastName;

    @FindBy(id = "inputUsername")
    private WebElement inputUsername;

    @FindBy(id = "inputPassword")
    private WebElement inputPassword;

    @FindBy(id = "signupButton")
    private WebElement signUpButton;

    @FindBy(id = "loginButton")
    private WebElement loginButton;

    public SignUpPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getInputUsername() {
        return inputUsername.getText();
    }

    public String getInputPassword() {
        return inputPassword.getText();
    }

    public String getInputFirstName() {
        return inputFirstName.getText();
    }

    public String getInputLastName() {
        return inputLastName.getText();
    }

    public void signUp(String firstName, String lastName, String username, String password) {
        inputFirstName.clear();
        inputLastName.clear();
        inputUsername.clear();
        inputPassword.clear();
        inputFirstName.sendKeys(firstName);
        inputLastName.sendKeys(lastName);
        inputUsername.sendKeys(username);
        inputPassword.sendKeys(password);
        signUpButton.click();
    }

}
