package com.shdx.learning.sb.cloudstorage.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    private final WebDriverWait wait;

    @FindBy(id = "logout-button")
    private WebElement logoutButton;

    @FindBy(id = "nav-notes-tab")
    private WebElement notesTab;

    @FindBy(id = "add-note-button")
    private WebElement addNoteButton;

    @FindBy(id = "edit-note-button")
    private WebElement editNoteButton;

    @FindBy(id = "save-note-button")
    private WebElement saveNoteButton;

    @FindBy(id = "delete-note-button")
    private WebElement deleteNoteButton;

    @FindBy(id = "note-title")
    private WebElement noteTitle;

    @FindBy(id = "note-description")
    private WebElement noteDescription;

    @FindBy(id = "nav-credentials-tab")
    private WebElement credentialsTab;

    @FindBy(id = "add-credential-button")
    private WebElement addCredentialButton;

    @FindBy(id = "edit-credential-button")
    private WebElement editCredentialButton;

    @FindBy(id = "save-credential-button")
    private WebElement saveCredentialButton;

    @FindBy(id = "close-credential-button")
    private WebElement closeCredentialButton;

    @FindBy(id = "delete-credential-button")
    private WebElement deleteCredentialButton;

    @FindBy(id = "credential-url")
    private WebElement credentialUrl;

    @FindBy(id = "credential-username")
    private WebElement credentialUsername;

    @FindBy(id = "credential-password")
    private WebElement credentialPassword;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 10);
    }

    public WebElement getNoteTitle() {
        return noteTitle;
    }

    public WebElement getNoteDescription() {
        return noteDescription;
    }

    public void logout() {
        logoutButton.click();
    }

    public void addNote(String title, String description) {
        notesTab.click();
        wait.until(ExpectedConditions.elementToBeClickable(addNoteButton));

        addNoteButton.click();
        wait.until(ExpectedConditions.visibilityOf(noteTitle));

        saveNote(title, description);
    }

    public void editNote(String title, String description) {
        notesTab.click();
        wait.until(ExpectedConditions.elementToBeClickable(editNoteButton));

        editNoteButton.click();
        wait.until(ExpectedConditions.visibilityOf(noteTitle));

        saveNote(title, description);
    }

    public void deleteNote() {
        notesTab.click();
        wait.until(ExpectedConditions.elementToBeClickable(deleteNoteButton));

        deleteNoteButton.click();
    }

    private void saveNote(String title, String description) {
        noteTitle.clear();
        noteDescription.clear();
        noteTitle.sendKeys(title);
        noteDescription.sendKeys(description);
        saveNoteButton.click();
    }

    public void addCredential(String url, String username, String password) {
        credentialsTab.click();
        wait.until(ExpectedConditions.elementToBeClickable(addCredentialButton));

        addCredentialButton.click();
        wait.until(ExpectedConditions.visibilityOf(credentialUrl));

        saveCredential(url, username, password);
    }

    public void editCredential(String url, String username, String password) {
        credentialsTab.click();
        wait.until(ExpectedConditions.elementToBeClickable(editCredentialButton));

        editCredentialButton.click();
        wait.until(ExpectedConditions.visibilityOf(credentialUrl));

        saveCredential(url, username, password);
    }

    public void deleteCredential() {
        credentialsTab.click();
        wait.until(ExpectedConditions.elementToBeClickable(deleteCredentialButton));

        deleteCredentialButton.click();
    }

    public void openCredentialModal() {
        editCredentialButton.click();
        wait.until(ExpectedConditions.visibilityOf(credentialPassword));
    }

    public void closeCredentialModal() {
        closeCredentialButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(credentialsTab));
    }

    private void saveCredential(String url, String username, String password) {
        credentialUrl.clear();
        credentialUsername.clear();
        credentialPassword.clear();
        credentialUrl.sendKeys(url);
        credentialUsername.sendKeys(username);
        credentialPassword.sendKeys(password);
        saveCredentialButton.click();
    }

}
