package com.shdx.learning.sb.cloudstorage;

import com.shdx.learning.sb.cloudstorage.page.HomePage;
import com.shdx.learning.sb.cloudstorage.page.LoginPage;
import com.shdx.learning.sb.cloudstorage.page.SignUpPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthenticationTests {

	private static final String BASE_URL = "http://localhost";
	private static final String LOGIN_PATH = "/login";
	private static final String SIGNUP_PATH = "/signup";
	private static final String HOME_PATH = "/home";
	private static WebDriver driver;
	private static SignUpPage signUpPage;
	private static LoginPage loginPage;
	private static HomePage homePage;

	@LocalServerPort
	private int port;

	@BeforeAll
	public static void beforeAll() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		signUpPage = new SignUpPage(driver);
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
	}

	@AfterAll
	public static void afterAll() {
		driver.quit();
	}

	@Test
	void testUnauthorizedUserAccess() {
		// Test access to login page
		driver.get(BASE_URL + ":" + port + LOGIN_PATH);
		Assertions.assertEquals("Login", driver.getTitle());

		// Test access to signup page
		driver.get(BASE_URL + ":" + port + SIGNUP_PATH);
		Assertions.assertEquals("Sign Up", driver.getTitle());

		// Test restricted access to home page (redirect to login page)
		driver.get(BASE_URL + ":" + port + HOME_PATH);
		Assertions.assertEquals("Login", driver.getTitle());
	}

	@Test
	void testSignUpLoginLogoutFlow() {
		// Sign up
		driver.get(BASE_URL + ":" + port + SIGNUP_PATH);
		signUpPage.signUp("John", "Doe", "j.doe", "123");

		// Test login
		driver.get(BASE_URL + ":" + port + LOGIN_PATH);
		loginPage.login("j.doe", "123");
		Assertions.assertEquals("Home", driver.getTitle());

		// Test logout
		homePage.logout();
		Assertions.assertEquals("Login", driver.getTitle());

		// Test restricted access after logout
		driver.get(BASE_URL + ":" + port + HOME_PATH);
		Assertions.assertEquals("Login", driver.getTitle());
	}

}
