package com.shdx.learning.sb.cloudstorage.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    @FindBy(id = "inputUsername")
    private WebElement inputUsername;

    @FindBy(id = "inputPassword")
    private WebElement inputPassword;

    @FindBy(id = "loginButton")
    private WebElement loginButton;

    @FindBy(id = "signupLink")
    private WebElement signupLink;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getInputUsername() {
        return inputUsername.getText();
    }

    public String getInputPassword() {
        return inputPassword.getText();
    }

    public void login(String username, String password) {
        inputUsername.clear();
        inputPassword.clear();
        inputUsername.sendKeys(username);
        inputPassword.sendKeys(password);
        loginButton.click();
    }

    public void signup() {
        signupLink.click();
    }
}